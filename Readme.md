This code repository contains an Ansible playbook to completely automate the installation and configuration of Nexus. 
----------------------------------------

- The first play installs Java 8 (a pre-req for Nexus to run) and net-tools so that the netstat -lpnt Linux command can be run.
- The second play downloads and unpacks the Nexus installer. The nexus-3 folder is renamed to “nexus,” and the task to untar the Nexus installer is skipped if the folder exists.
- The third play creates a nexus user, a nexus group, and assigns nexus as the owner of the Nexus-related folders
- The fourth play updates the nexus.rc configuration file by specifying nexus as the “run as user” and starts Nexus as the nexus user via a command
 - The last play confirms Nexus is running by using ps aux | grep nexus and netstat as diagnostic tools
